// Copyright 2022 hjiayz
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import * as parser from "./server/src/parser.js";




const val = `
    // xxxx
//  dddd
/// xxxd
    abc = 123..123
    abd = ..
    abe = 123..
    abf = ..123
    abg = 0xFF.. 
        | .. 
        | 0xF0.. 
        | -1
        & 1..100
    b   = bytes
    c   = f32
    d   = f64
    e   = 123
    bytes = 123
    x   = hhh
    s   = /abc\\\\/i
    obj = a : b
        & c : d
        & e : f

    uni = a : b
        | c : d
        | e : f

    sig = a : x

    bs  = a : b
        | c : d
        & e : f
    
    bd  = a : b
        | a : c

    ua  = a : b[1..]
        | c : d
        | e : f[2]

    ali = abc 
    alj = abc[1..]
        
`;

BigInt.prototype.toJSON = function(){
    return this.toString();
};

const p = parser.parse(val);

console.log(JSON.stringify(p,null,8));