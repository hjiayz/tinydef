

import {
	createConnection,
	TextDocuments,
	Diagnostic,
	DiagnosticSeverity,
	ProposedFeatures,
	InitializeParams,
	DidChangeConfigurationNotification,
	CompletionItem,
	CompletionItemKind,
	TextDocumentPositionParams,
	TextDocumentSyncKind,
	InitializeResult,
	DocumentHighlightParams,
	DocumentHighlight,
	HandlerResult
} from 'vscode-languageserver/node';


import { TDocument } from "./mgr";

// Create a connection for the server, using Node's IPC as a transport.
// Also include all preview / proposed LSP features.
const connection = createConnection(ProposedFeatures.all);

const manager: Record<string, TDocument> = {};

let hasConfigurationCapability = false;
let hasWorkspaceFolderCapability = false;
let hasDiagnosticRelatedInformationCapability = false;

connection.onInitialize((params: InitializeParams) => {
	const capabilities = params.capabilities;

	// Does the client support the `workspace/configuration` request?
	// If not, we fall back using global settings.
	hasConfigurationCapability = !!(
		capabilities.workspace && !!capabilities.workspace.configuration
	);
	hasWorkspaceFolderCapability = !!(
		capabilities.workspace && !!capabilities.workspace.workspaceFolders
	);
	hasDiagnosticRelatedInformationCapability = !!(
		capabilities.textDocument &&
		capabilities.textDocument.publishDiagnostics &&
		capabilities.textDocument.publishDiagnostics.relatedInformation
	);

	const result: InitializeResult = {
		capabilities: {
			textDocumentSync: TextDocumentSyncKind.Full,
			// Tell the client that this server supports code completion.
			completionProvider: {
				resolveProvider: true
			},
			documentHighlightProvider: true
		}
	};
	if (hasWorkspaceFolderCapability) {
		result.capabilities.workspace = {
			workspaceFolders: {
				supported: true
			}
		};
	}
	return result;
});

connection.onInitialized(() => {
	if (hasConfigurationCapability) {
		// Register for all configuration changes.
		connection.client.register(DidChangeConfigurationNotification.type, undefined);
	}
	if (hasWorkspaceFolderCapability) {
		connection.workspace.onDidChangeWorkspaceFolders(_event => {
			connection.console.log('Workspace folder change event received.');
		});
	}
});


// Cache the settings of all open documents

connection.onDidChangeConfiguration(change => {
	connection.console.log(JSON.stringify(change));
});


connection.onDidChangeWatchedFiles(_change => {
	// Monitored files have change in VSCode
	connection.console.log('We received an file change event');
	connection.console.log(JSON.stringify(_change));
});

connection.onDocumentHighlight((documentHighlightParams: DocumentHighlightParams) => {
	connection.console.log('onDocumentHighlight');
	connection.console.log(JSON.stringify(documentHighlightParams));
	let x: HandlerResult<DocumentHighlight[] | null | undefined, void> = [];
	return x;
});

// This handler provides the initial list of the completion items.
connection.onCompletion(
	(_textDocumentPosition: TextDocumentPositionParams): CompletionItem[] => {
		// The pass parameter contains the position of the text document in
		// which code complete got requested. For the example we ignore this
		// info and always provide the same completion items.
		return [
			{
				label: 'TypeScript',
				kind: CompletionItemKind.Text,
				data: 1
			},
			{
				label: 'JavaScript',
				kind: CompletionItemKind.Text,
				data: 2
			}
		];
	}
);


// This handler resolves additional information for the item selected in
// the completion list.
connection.onCompletionResolve(
	(item: CompletionItem): CompletionItem => {
		if (item.data === 1) {
			item.detail = 'TypeScript details';
			item.documentation = 'TypeScript documentation';
		} else if (item.data === 2) {
			item.detail = 'JavaScript details';
			item.documentation = 'JavaScript documentation';
		}
		return item;
	}
);



// Make the text document manager listen on the connection
// for open, change and close text document events
// documents.listen(connection);

connection.onDidOpenTextDocument((e) => {

	connection.console.info(JSON.stringify(e));
	manager[e.textDocument.uri] = new TDocument(e.textDocument.text, e.textDocument.uri, e.textDocument.languageId, e.textDocument.version,(v:string)=>connection.console.info(v))
	manager[e.textDocument.uri].tree.forEach((v)=>connection.console.info(v.toString()));
	connection.sendDiagnostics(manager[e.textDocument.uri].diagnostic());
})

connection.onDidChangeTextDocument(e=>{
	connection.console.info(JSON.stringify(e));
	manager[e.textDocument.uri].update(e.contentChanges,e.textDocument)
	connection.sendDiagnostics(manager[e.textDocument.uri].diagnostic());
})

connection.onDidCloseTextDocument(e=>{
	delete manager[e.textDocument.uri];
})

// Listen on the connection
connection.listen();
