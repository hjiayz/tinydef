// Copyright 2022 hjiayz
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

export type SimpleKind = {
	kind: "f32" | "f64" | "bytes"
}

export type NumKind = {
	kind: "number",
	list: {
		op?:"union"| "intersection",
		start?:bigint,
		end?:bigint,
	}[]
}

export type RegexKind = {
	kind: "string",
	regex: "string",
	regex_params : {
		case_insensitive : boolean
	}
}

export type Def = {
	kind: string,
	kind_position:Range,
	name_position:Range,
	is_array:boolean,
}

export type CompositeKind = {
	kind:"union"| "struct",
	props:Record<string,Def>,
}

export type AliasKind = {
	kind:"alias",
	alias:Def,
}

export type Define = SimpleKind | NumKind | RegexKind | CompositeKind | AliasKind

export type Position = {
	offset: number,
	line: number,
	column: number
}

export type Range = {
	start:Position,
	end:Position
}

export type Error = {
	position:Range,
	message:string,
}

export type Highlight = {
	kind : string,
	position : Range,
}

export type Block = {
	ident? : string,
	define? : Define,
	annotations? : string[],
	errors? : Error[],
	highlights? : Highlight[],
	position : Range,
	ident_position? : Range,
	lineoffsets? : number[],
}


export let parse: (input: string, options: any) => Block[];