// Copyright 2022 hjiayz
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


import { Diagnostic, DiagnosticSeverity, Position, PublishDiagnosticsParams, TextDocumentContentChangeEvent, VersionedTextDocumentIdentifier } from 'vscode-languageserver/node.js';
import { parse,Block,Error } from "./parser.js";

export class TDocument {
	update(contentChanges: TextDocumentContentChangeEvent[], textDocument: VersionedTextDocumentIdentifier) {
		if (textDocument.uri!==this.uri) {
			return;
		}
		this.tree = this.tree || [];
		contentChanges.forEach(content=>updateTree(content,this.tree));
		this.version=textDocument.version
	}
	tree : Block[];
	uri : string;
	languageId : string;
	version : number;
	logger: (v: string) => void;
	constructor(text:string,uri:string,languageId:string,version:number,logger: (v: string) => void){
		this.tree = parse(text,{});
		this.uri = uri;
		this.languageId = languageId;
		this.version = version;
		this.logger = logger;
	}
	diagnostic():PublishDiagnosticsParams {
		let diagnostics = [];
		for (let block of this.tree) {
			for(let error of block.errors||[]) {
				this.logger(error.position.start.line.toString())
				this.logger(error.position.end.line.toString())
				this.logger(error.message)
				diagnostics.push(makediagnostic(error));
			}
		}
		return {
			uri:this.uri,
			version:this.version,
			diagnostics,
		}
	}
}

function makediagnostic(error:Error):Diagnostic{
	return {
		severity: DiagnosticSeverity.Error,
		range: {
			start: intopos(error.position.start.line,error.position.start.column),
			end: intopos(error.position.end.line,error.position.end.column),
		},
		message: error.message,
		source: 'tiny define language'
	}
}

function intopos(line:number,col:number):Position{
	return Position.create(line-1,col-1);
}

function updateTree(content: TextDocumentContentChangeEvent, tree: Block[]): void {
	if ("range" in content) {
		let start : [number,number] = [content.range.start.line+1,content.range.start.character+1];
		let end : [number,number] = [content.range.end.line+1,content.range.end.character+1];
		let startBlock = 0;
		for (let i = 0; i<tree.length; i++) {
			if ((tree[i].position.end.line<=start[0]) && (tree[i].position.end.column<=start[1])) {
				startBlock = i;
				break;
			}
			startBlock = i;
		}
		let endBlock = startBlock;
		for (let i = startBlock;i<tree.length;i++){
			if ((tree[i].position.start.line>=end[0]) && (tree[i].position.start.column>=end[1])) {
				endBlock = i;
				break;
			}
			endBlock = i+1;
		}
		for (let i = startBlock;i<=endBlock;i++) {

		}
		//todo
	}
	else {
		tree = parse(content.text,{});
	}
}
